const game = (function(){ 
    // variables are scoped to the function context
    
    let player; // X and O
    let grid = { 
        resolved: false, 
        data: [], 
        filledCells: 0, 
        winner: undefined, 
        gameOver: false 
    }; // hold the grid state
    
    function takeTurn(ev) {
        log('takeTurn');
        let board, cell, parts, x, y, me = ev.target;
        
        if ( grid.gameOver ) {
            return msg('The game is over. Refresh to restart.');
        }
        
        if ( me.innerHTML.match(/[XO]/ )) {
            return msg('That cell is already claimed. Try again.');
        }
        
        // toggle between X and O
        if (player == 'X') {
            player = 'O';
        } else {
            player = 'X';
        }
        
        // claim the cell with the player's mark
        me.setAttribute('owner', player);
        me.innerHTML = player; // rely on CSS for visual styling of the container
        grid.filledCells += 1;
        
        // check for win, or tie
        if (!grid.resolved) {
            // lets try to build the grid reference map once
            board = me.parentNode.parentNode; // table or tbody (enclosure for all the cells in this board)
            for (let i = 0, j = board.getElementsByTagName('td').length; i < j; i++) {
                cell = board.getElementsByTagName('td')[i];
                if (cell.id) {
                    parts = cell.id.split('_');
                    console.log('found ', cell.id, parts);
                    
                    // we're building an array of arrays on-the-fly (if we don't have the first (row), create it...)
                    if ( !grid.data[parts[1]] ) { grid.data.push([]) }
                    
                    // now, populate the (row x col) with a reference to the cell
                    grid.data[parts[1]][parts[2]] = cell;
                    
                    // associate an X and Y with each grid cell discovered
                    cell.setAttribute('y', parts[1]); // y is the row
                    cell.setAttribute('x', parts[2]); // x is the column
                }
            }
            grid.resolved = true;
        }
        
        // look for a win pattern (when there are enough filled cells that a win is possible)
        if ( grid.filledCells > 4 ) {
            // find x, y
            x = me.getAttribute('x');
            y = me.getAttribute('y');

            // scan known win patterns
            // check the x column
            if ( owns(x,0,player) && owns(x,1,player) && owns(x,2,player) ) {
                grid.winner = player;
            } 
            // check the y row
            else if ( owns(0,y,player) && owns(1,y,player) && owns(2,y,player) ) {
                grid.winner = player;
            } 
            // check x1 diagonal 
            else if ( owns(0,0,player) && owns(1,1,player) && owns(2,2,player) ) {
                grid.winner = player;
            } 
            // check y1 diagonal
            else if ( owns(0,2,player) && owns(1,1,player) && owns(2,0,player) ) {
                grid.winner = player;
            }
        }

        if ( grid.winner ) {
            grid.gameOver = true;
            return msg('Player ' + player + ' wins!');
        }
        
        if ( grid.filledCells == 9 ) {
            grid.gameOver = true;
            return msg('Game ended in a tie.');
        }
    }

    function owns(x,y,player) {
        log('owns');
        return grid.data[x][y].getAttribute('owner') === player;
    }

    function msg(str) {
        log('msg');
        setTimeout(function(){ 
            // settimeout allows UI to update before modal dialog appears
            alert(str); 
        }, 0); 
        return true;
    }
    
    function log() {
        console.log('From tic-tac-toe.js: ', ...arguments);
    }
    
    return {
       takeTurn: takeTurn
    }
    
})();