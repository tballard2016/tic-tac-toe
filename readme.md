Notes:

Lesson Plan:
1) Init Project
    -tools
    -workspace
    -git and a repo
    
2) First Pass
    -"You're doing it wrong!" (Yup! I sure am.)

3) Refining It
    -doing it better.


Diving Deeper:

0) I'm doing all of this from within VS Code. It's a great (free) integrated development tool that's very popular among professional developers at the time of this writing. 
    Hint: You don't actually need any special tools to do this. A simple text editor and access to windows explorer or a command line will also work.
    (So, why VS Code? I am using it because its convenient, and helps me to be faster, catch errors sooner, and its commonly used in teams that I have worked with--which means my teammates can help me level-up, share kung-fu tips, and lament missing features or nits that we want to hate on. ;-) Healthy social interaction stuff, ya know?)
    
1) First step is create a project space for the files you will use to build your game/project/site.
    On my dev computer I have a folder I use to hold projects I'm fiddling with. Open the terminal in VS Code to proceed.
        Windows: C:\Users\Ivan\websites\
        Linux: ~/websites/     
        
        Hint: On unix-based operating systems (linux, macOS, etc) "~" is your "home directory", 
        it's an easy shortcut if you navigate around on the command line. Try some examples:
        "cd ~",
        "ls ~/web(press tab)..."  (pressing the TAB key uses a feature called tab-complete and will find matches in the folder like "websites")
        
    So, I'm creating a folder in websites called "tic-tac-toe" to put my files in. And then I'm going to create an empty file called index.html.
        (open terminal in VS Code)
        mkdir ~/websites/tic-tac-toe/
        cd ~/websites/tic-tac-toe
        touch index.html
        
    Then
        
        tic-tac-toe\index.html
        
        
2) Create a GIT repo for the project.
    You don't have to do this, but I do. ;-)  (I cannot pitch a version control offsite backup enough. If you don't do something similar yet, learn to and embrace it. 'nuff said.)
    
    Steps I did:
    >cd into the folder with the project and run "git init"
    >I use bitbucket, so I created a new bit bucket repo using a browser, then I'm associating my git init with that repo...
        git remote add origin https://tballard2016@bitbucket.org/tballard2016/tic-tac-toe.git